import { connect } from "react-redux";
import { IState } from "../../reducers";
import { updateError } from "../../actions/user/user.actions";
import {
  addProduct,
  reinitializeProduct,
  setAuctionEnd,
  setBuyNow,
  setMinBid,
  setTimePosted,
  updateBidder,
  updateCategory,
  updateCondition,
  updateName,
  updateProductUsername,
  updatePhotos,
  updatePhotoNames,
  updateStatus,
  updateType
} from "../../actions/product/product.actions";
import { NewPostComponent } from "./new-post.component";

const mapStateToProps = (state: IState) => ({
  product: state.product,
  user: state.user
});

export const mapDispatchToProps = {
  addProduct,
  reinitializeProduct,
  setAuctionEnd,
  setBuyNow,
  setMinBid,
  setTimePosted,
  updateBidder,
  updateCategory,
  updateCondition,
  updateError,
  updateName,
  updatePhotoNames,
  updatePhotos,
  updateProductUsername,
  updateStatus,
  updateType
};

export const NewPostContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPostComponent);
